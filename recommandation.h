/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#if !defined(__RECOMMANDATION_H__)
#define __RECOMMANDATION_H__

class Personne;
#include <iostream>
#include "personne.h"
#include "tableau.h"
class Recommandation {
	public:
		void creer_recommandation(const Tableau<Personne> &personnes) ;
		void creer_recommendation_seul(const Personne& p);
	private:
		std::string recommandation;
		void creer_recommandation_allee(const Tableau<Personne>& personnes);
		void creer_recommandation_retour(const Tableau<Personne>& personnes);
		friend std::ostream& operator << (std::ostream&, const Recommandation&);
};

#endif


/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département destination'informatique          *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/             
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504	
 *  */

#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include "personne.h"
#include "tableau.h"
#include "verificateur_horaire.h"

int tp1(std::istream& entree);
void afficher_recommandation(Personne& p, double cout);
int construire_tableau_personne_par_fichier(std::istream& entree, Tableau<Personne> *tableau);
double calculer_cout_sans_partage(const Personne&, const Personne&);
double calculer_cout_avec_partage(const Personne&, const Personne&);
bool partage_est_optimal(const double distance_sans, const double distance_avec);
bool ont_horaire_compatible_sans_partage(const Personne& conducteur, const Personne& covoitureur);
bool ont_horaire_compatible_avec_partage(const Personne& conducteur, const Personne& covoitureur);
// argc: nombre destination'arguments passés.
// argv: tableau de chaines de carractères.
// exemple: ./tp1 test00.txt
//   ==> argc=2; argv[0]="./tp1"; argv[1]="test00.txt"
int main(int argc, const char** argv)
{
	if(argc>1){
		std::ifstream fichier_entree(argv[1]);
		if(fichier_entree.fail()){
			std::cerr << "Erreur de lecture du fichier '" << argv[1] << "'" << std::endl;
			return 1;
		}
		return tp1(fichier_entree);
	}else{
		// Pas destination'argument requêtes ==> lecture depuis l'entrée standard avec std::cin.
		return tp1(std::cin);
	}
}

int tp1(std::istream& entree_requetes){
	//Structure pour stocker les personnes. Indice: on devrait utiliser le Tableau.
	Tableau<Personne> personnes;
	Verificateur_horaire verificateur_horaire;
	//Lecture
	construire_tableau_personne_par_fichier(entree_requetes, &personnes);	
	int taille = personnes.taille();
	// Parcour la liste de personne sous la perspective du conducteur.
	for (int i = 0; i < taille;  ++i) {
		Personne *conducteur = &personnes[i];
		Recommandation r;
		r.creer_recommendation_seul(*conducteur);
		conducteur->recommandation = &r;
		double cout_voyager_seul = 2 * distance(conducteur->origine, conducteur->destination);
		double meilleur_cout = cout_voyager_seul;	
		double dist = 0;
		bool devrait_partager_vehicule = false;
		// Parcour de la liste de personne pour trouver un covoitureur
		for (int j = 0; j < taille; ++j) {
			if(i != j) {
				Personne *covoitureur = &personnes[j];
				double cout_covoitureur = 2 * distance(covoitureur->origine, covoitureur->destination);
				double cout_sans_partage = calculer_cout_sans_partage(*conducteur, *covoitureur);
				double cout_avec_partage = cout_sans_partage + 1; 

				if(conducteur->peut_preter) {
					cout_avec_partage = calculer_cout_avec_partage(*conducteur, *covoitureur);	
					devrait_partager_vehicule = partage_est_optimal(cout_sans_partage, cout_avec_partage);
				}
				// On vérifie tout de suite si voyager seul est une meilleure option.
				if(cout_voyager_seul < (cout_sans_partage - cout_covoitureur) && cout_voyager_seul < (cout_avec_partage - cout_covoitureur)) continue;	

				// Vérification de la compatibilité des horaires
				bool horaire_compatible_sans_partage = verificateur_horaire.ont_horaire_compatible_sans_partage(*conducteur, *covoitureur);
				bool horaire_compatible_avec_partage = verificateur_horaire.ont_horaire_compatible_avec_partage(*conducteur, *covoitureur);

				// Si le conducteur ne peut pas prêter et que son horaire est non compatible sans partage
				// ou que aucun des horaires n'est compatibles, on passe au suivant.
				if((!conducteur->peut_preter && !horaire_compatible_sans_partage) || (conducteur->peut_preter && !horaire_compatible_avec_partage && !horaire_compatible_sans_partage)) continue;	
				devrait_partager_vehicule = (devrait_partager_vehicule && horaire_compatible_avec_partage) || (!horaire_compatible_sans_partage && horaire_compatible_avec_partage);

				// Si on ne peut pas partager le véhicule et que l'horaire est incompatible
				// sans partage, on passe au suivant.
				if(!devrait_partager_vehicule && !horaire_compatible_sans_partage) continue;

				dist = devrait_partager_vehicule ? cout_avec_partage : cout_sans_partage;
				double cout = dist - cout_covoitureur; 

				// Modification de la meilleure recommandation si le cout est meilleur.
				if(cout < meilleur_cout){
					Tableau<Personne> passagers;
					passagers.ajouter(*conducteur);	
					passagers.ajouter(*covoitureur);

					if(devrait_partager_vehicule) {
						passagers.ajouter(*conducteur);
						passagers.ajouter(*covoitureur);
					} else {
						passagers.ajouter(*covoitureur);
						passagers.ajouter(*conducteur);
					}

					meilleur_cout = cout;
					r.creer_recommandation(passagers);
					conducteur->recommandation = &r; 
				}
			}

		}	
		afficher_recommandation(*conducteur, meilleur_cout);
	}
	return 0;
}

// Affiche la recommandation considéré comme étant la meilleure.
void afficher_recommandation(Personne& conducteur, double cout) {
	std::cout << conducteur
		<< round(cout)	
		<< "m" << std::endl;
}


// Construire un tableau de personnes à partir d'un fichier texte.
int construire_tableau_personne_par_fichier(std::istream& entree_requetes, Tableau<Personne> *personnes) {
	while(entree_requetes && !entree_requetes.eof()){
		Personne p;
		// Lecture d'une personne
		entree_requetes >> p >> std::ws;
		personnes->ajouter(p); 
	}
	return 0;
}

// Calcul le coût d'un trajet en mètres sans que le conducteur ne partage son véhicule.
double calculer_cout_sans_partage(const Personne& conducteur, const Personne& covoitureur) {
	double distance_allee = 
		distance(conducteur.origine, covoitureur.origine);	

	distance_allee += distance(covoitureur.origine, covoitureur.destination);
	distance_allee += distance(covoitureur.destination, conducteur.destination);

	return 2 * distance_allee;
}

// Calcul le coût d'un trajet en mètres en considérant que le conducteur partage son véhicule.
double calculer_cout_avec_partage(const Personne& conducteur, const Personne& covoitureur) {
	double distance_allee = 
		distance(conducteur.origine, covoitureur.origine);

	distance_allee += distance(covoitureur.origine, conducteur.destination);
	distance_allee	+= distance(conducteur.destination, covoitureur.destination);

	return 2 * distance_allee;
}

// Vérifie si partager le véhicule est une solution optimale.
bool partage_est_optimal(const double cout_sans_partage, const double cout_avec_partage) {
	return cout_avec_partage < cout_sans_partage;
}

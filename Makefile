# Makefile pour INF3105 / TP1.
# Adaptez ce fichier si nécessaire.

# Choisir l'une des ces trois configurations:
#OPTIONS = -Wall
#OPTIONS = -g -O0 -Wall
OPTIONS = -O2 -Wall
all : tp1

tp1 : tp1.cpp heure.o pointst.o personne.o tableau.h vehicule.o verificateur_horaire.o recommandation.o
	g++ ${OPTIONS} -o tp1 tp1.cpp heure.o personne.o pointst.o vehicule.o verificateur_horaire.o recommandation.o
	
pointst.o : pointst.cpp pointst.h
	g++ ${OPTIONS} -c -o pointst.o pointst.cpp

heure.o : heure.cpp heure.h
	g++ ${OPTIONS} -c -o heure.o heure.cpp

recommandation.o : recommandation.cpp recommandation.h 
	g++ ${OPTIONS} -c -o recommandation.o recommandation.cpp

personne.o : personne.cpp personne.h pointst.h 
	g++ ${OPTIONS} -c -o personne.o personne.cpp
verificateur_horaire.o : verificateur_horaire.cpp verificateur_horaire.h
	g++ ${OPTIONS} -c -o verificateur_horaire.o verificateur_horaire.cpp
vehicule.o : vehicule.cpp vehicule.h
	g++ ${OPTIONS} -c -o vehicule.o vehicule.cpp

clean:
	rm -rf tp1 *~ *.o


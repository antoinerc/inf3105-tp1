/* INF3105 - Structures de données et algorithmes        *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#if !defined(__VERIFICATEUR_HORAIRE_H__)
#define __VERIFICATEUR_HORAIRE_H__
#include "personne.h"

class Verificateur_horaire {
	public:
		bool ont_horaire_compatible_avec_partage(const Personne& conducteur, const Personne& covoitureur) const;
		bool ont_horaire_compatible_sans_partage(const Personne& conducteur, const Personne& covoitureur) const;
	private:
		Heure determiner_heure_depart(const Heure& depart_conducteur, const Heure& depart_covoitureur, const Heure& distance_temps) const;
		void determiner_temps_requis_entre_destinations(const Personne& conducteur, const Personne& covoitureur, Tableau<Heure> *heures, const bool avec_partage = false) const ;
};

#endif

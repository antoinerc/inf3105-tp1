/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/ 
 *	
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#include <string>
#include "recommandation.h"

// Créer une recommandation pour une personne seule.
void Recommandation::creer_recommendation_seul(const Personne& p) {
	recommandation = "";
	recommandation 
		+= "+" + p.nom
		+ "-"+ p.nom
		+ "\t"
		+ "+"+ p.nom
		+ "-"+ p.nom;
}

// Créer une recommandation.
void Recommandation::creer_recommandation(const Tableau<Personne>& ordre_passagers) {
	recommandation = "";
	creer_recommandation_allee(ordre_passagers);
	recommandation += "\t";
	creer_recommandation_retour(ordre_passagers);
}

// Créer la recommandation pour l'allée.
void Recommandation::creer_recommandation_allee(const Tableau<Personne>& ordre_passagers){
	int taille = ordre_passagers.taille();
	for(int i = 0; i < taille ; i++){ 
		if(i < taille / 2) 
			recommandation += "+" + ordre_passagers[i].nom;
		else 
			recommandation += "-" +ordre_passagers[i].nom;	
	}
}

// Créer la recommandation pour le retour.
void Recommandation::creer_recommandation_retour(const Tableau<Personne>& ordre_passagers){
	// On parcour la liste à l'envers.
	int taille = ordre_passagers.taille();
	for(int i = taille - 1; i >= 0; i--){
		if(i >= taille/ 2) 
			recommandation += "+" + ordre_passagers[i].nom;
		else 
			recommandation += "-" + ordre_passagers[i].nom;
	}
	
}

// Overwrite de la fonction d'output.
std::ostream& operator << (std::ostream& os, const Recommandation& r) {
	os << r.recommandation;
	return os;
}


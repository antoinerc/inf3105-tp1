/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */


#include <cassert>
#include <cstdlib>
#include <cstdio>
#include "heure.h"
#include <math.h>
Heure::Heure(){};

// Construit une Heure à partir d'une quantité de secondes
Heure::Heure(const double secondes) {
	this->secondes = secondes;
}

// Construit une "heure" en secondes à partir d'heures et minutes.
Heure::Heure(const int heures, const int minutes){
	this->secondes = (heures * 3600 + minutes * 60);
}

// Comportement pour l'opérateur <
bool Heure::operator <(const Heure& h) const{
	return this->secondes < h.secondes;
}

// Comportement de l'opérateur + 
const Heure Heure::operator +(const Heure& h) const{
	return this->secondes + h.secondes;
}

// Construit une date à la lecture.
std::istream& operator >> (std::istream& is, Heure& heure){
	char h;
	int heures;
	int minutes;
	is >> heures >> h >> minutes;
	heure = Heure(heures, minutes);
	if(!is) return is;
	assert(h=='h' || h=='H'); // pour robustesse: valider le caractère 'h'
	return is;
}


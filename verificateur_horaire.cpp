/* INF3105 - Structures de données et algorithmes       *
 * UQAM / Département d'informatique                    *
 * Automne 2018 / TP1                                   *
 * http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 * AUTEUR: ANTOINE-RICHER-CLOUTIER / RICA11129504
 * */


#include "verificateur_horaire.h"

// Valide si deux horaires sont compatible en considérant que les covoitureurs partageront le véhicule.
bool Verificateur_horaire::ont_horaire_compatible_avec_partage(const Personne& conducteur, const Personne& covoitureur) const {
	Heure somme_trajet;
	Tableau<Heure> temps_requis; 
	determiner_temps_requis_entre_destinations(conducteur, covoitureur, &temps_requis, true);	

	// Allée
	somme_trajet = determiner_heure_depart(conducteur.depart_origine, covoitureur.depart_origine, temps_requis[0]);

	somme_trajet = somme_trajet + temps_requis[1];
	if(conducteur.arrivee_destination < somme_trajet) return false; 

	somme_trajet = somme_trajet + temps_requis[2];
	if(covoitureur.arrivee_destination < somme_trajet) return false;

	// Retour
	somme_trajet = determiner_heure_depart(covoitureur.depart_destination, conducteur.depart_destination, temps_requis[2]);

	somme_trajet = somme_trajet + temps_requis[1];

	if(covoitureur.arrivee_origine < somme_trajet) return false;

	somme_trajet = somme_trajet + temps_requis[0];

	if(conducteur.arrivee_origine < somme_trajet) return false;

	return true;
}

// Valide si deux horaires sont compatible en considérant que les covoitureurs ne se partageront pas le véhicule.
bool Verificateur_horaire::ont_horaire_compatible_sans_partage(const Personne& conducteur, const Personne& covoitureur) const  {
	Tableau<Heure> temps_requis; 
	determiner_temps_requis_entre_destinations(conducteur, covoitureur, &temps_requis);	
	Heure somme_trajet;

	// Allée
	somme_trajet = determiner_heure_depart(conducteur.depart_origine, covoitureur.depart_origine, temps_requis[0]);

	somme_trajet = somme_trajet + temps_requis[1];
	if(covoitureur.arrivee_destination < somme_trajet) return false;

	somme_trajet = somme_trajet + temps_requis[2];

	if(conducteur.arrivee_destination < somme_trajet) return false;

	// Retour
	somme_trajet = determiner_heure_depart(conducteur.depart_destination, covoitureur.depart_destination, temps_requis[2]);
	somme_trajet = somme_trajet + temps_requis[1];
	if(covoitureur.arrivee_origine < somme_trajet) return false;

	somme_trajet = somme_trajet + temps_requis[0];

	if(conducteur.arrivee_origine < somme_trajet) return false;

	return true;
}

// Détermine si on doit choisir l'heure de départ du covoitureur ou  la somme de
// la distance et de l'heure de départ du conducteur.
Heure Verificateur_horaire::determiner_heure_depart(const Heure& depart_conducteur, const Heure& depart_covoitureur, const Heure& distance_temps) const  {
	return depart_conducteur + distance_temps < depart_covoitureur ? depart_covoitureur : (depart_conducteur + distance_temps); 
}

// Détermine le temps requis entre les destinations en termes d'heures.
void Verificateur_horaire::determiner_temps_requis_entre_destinations(const Personne& conducteur, const Personne& covoitureur, Tableau<Heure> *heures, const bool avec_partage) const {
	heures->ajouter(Heure(conducteur.vehicule.calculer_temps_requis_distance_en_secondes(
					distance(conducteur.origine, covoitureur.origine)))); 
	if(avec_partage) {
		heures->ajouter(Heure(conducteur.vehicule.calculer_temps_requis_distance_en_secondes(
						distance(covoitureur.origine, conducteur.destination))));
		heures->ajouter(Heure(conducteur.vehicule.calculer_temps_requis_distance_en_secondes(
						distance(conducteur.destination, covoitureur.destination))));
	} else {
		heures->ajouter(Heure(conducteur.vehicule.calculer_temps_requis_distance_en_secondes(
						distance(covoitureur.origine, covoitureur.destination))));
		heures->ajouter(Heure(conducteur.vehicule.calculer_temps_requis_distance_en_secondes(
						distance(covoitureur.destination, conducteur.destination))));
	}
}	

/* INF3105 - Structures de données et algorithmes       *
 * UQAM / Département d'informatique                    *
 * Automne 2018 / TP1                                   *
 * http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 * AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 * */

#include "vehicule.h"
#include <math.h>
Vehicule::Vehicule(){ }
Vehicule::Vehicule(const int nb_places, const int vitesse) 
{
	this->nb_places = nb_places;
	this->vitesse = vitesse;
}

// Fonction qui calcul le temp requis en secondes pour parcourir la distance selon la vitesse du véhicule.
double Vehicule::calculer_temps_requis_distance_en_secondes(const double distance) const {
	return distance / vitesse;
}

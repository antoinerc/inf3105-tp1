/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département destination'informatique          *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#include <cassert>
#include "personne.h"

// Lecture d'une personne
std::istream& operator >> (std::istream& is, Personne& p){
	// À compléter.
	std::string s;
	int n;
	char c;
	char peut_preter;
	// lire nom, nombre de passagers, l'acceptation de prêter le véhicule
	is >> p.nom >> n >> peut_preter;
	p.peut_preter = peut_preter == 'O' ? true : false;
	p.vehicule = Vehicule(n);
	// lire l'origine (résidence) et la destination (travail / étude)
	is >> p.origine >> s >> p.destination;
	assert(s == "-->"); // robustesse
	// lire les contraintes de temps (4 heures)
	is >> p.depart_origine >> p.arrivee_destination 
		>> p.depart_destination >> p.arrivee_origine;
	// lire le point-virgule de fin de ligne
	is >> c;
	assert(c == ';'); // robustesse
	return is;
}

// Affichage d'une ligne de recommendation.
std::ostream& operator << (std::ostream& os, const Personne& p){
	// À compléter.
	os << p.nom << ":\t" << *p.recommandation << "\t";
	return os;
}


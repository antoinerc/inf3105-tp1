/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#if !defined(__HEURE_H__)
#define __HEURE_H__

#include <iostream>

class Heure{
	public:
		Heure();
		Heure(const double secondes);
		Heure(const int heures, const int minutes);
		bool operator <(const Heure& h) const;
		const Heure operator +(const Heure& h) const;
	private:
		int secondes;
		friend std::istream& operator >> (std::istream&, Heure&);
		friend std::ostream& operator << (std::ostream&, const Heure&);
};

#endif


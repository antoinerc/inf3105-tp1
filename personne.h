/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/             
 *  
 *  AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#if !defined(__PERSONNE_H__)
#define __PERSONNE_H__

class Recommandation;
#include <iostream>
#include "heure.h"
#include "pointst.h"
#include "vehicule.h"
#include "tableau.h"
#include "recommandation.h"

class Personne{
	public:
		PointST origine, destination;
		std::string nom;
		Heure depart_origine;
		Heure arrivee_destination;
		Heure depart_destination;
		Heure arrivee_origine;	
		bool peut_preter;
		Vehicule vehicule;	
		Recommandation* recommandation;
	private:
		friend std::istream& operator >> (std::istream&, Personne&);
		friend std::ostream& operator << (std::ostream&, const Personne&);
};

#endif


/* INF3105 - Structures de données et algorithmes        *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              
 *	
 *	AUTEUR: ANTOINE RICHER-CLOUTIER / RICA11129504
 *  */

#if !defined(__VEHICULE_H__)
#define __VEHICULE_H__

class Vehicule{
	public:
		Vehicule();
		Vehicule(const int nb_places, const int vitesse = 10);
		double calculer_temps_requis_distance_en_secondes(const double distance) const;
	private:
		int nb_places;
		int vitesse;
};

#endif
